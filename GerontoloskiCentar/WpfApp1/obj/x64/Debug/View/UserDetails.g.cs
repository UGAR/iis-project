﻿#pragma checksum "..\..\..\..\View\UserDetails.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "63F4C3A52B9B00DA15B0DFE2BC222DC5DF6F17FF"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfApp1.View;


namespace WpfApp1.View {
    
    
    /// <summary>
    /// UserDetails
    /// </summary>
    public partial class UserDetails : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 8 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfApp1.View.UserDetails ud;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image PortraitImage;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Prezime;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Ime;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label ImeRoditelja;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label DatumRodjenja;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label MestoOpstinaDrzava;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Prebivaliste;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Jmbg;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Pol;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox mobilnost;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox szr;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Izdaje;
        
        #line default
        #line hidden
        
        
        #line 86 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label BrojDokumenta;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label DatumIzadavanja;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label VaziDo;
        
        #line default
        #line hidden
        
        
        #line 99 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox placanje;
        
        #line default
        #line hidden
        
        
        #line 102 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label sml;
        
        #line default
        #line hidden
        
        
        #line 103 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox smestaj;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label cenaSmestaja;
        
        #line default
        #line hidden
        
        
        #line 112 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox hobi;
        
        #line default
        #line hidden
        
        
        #line 116 "..\..\..\..\View\UserDetails.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox ishrana;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WpfApp1;component/view/userdetails.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\View\UserDetails.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.ud = ((WpfApp1.View.UserDetails)(target));
            return;
            case 2:
            this.PortraitImage = ((System.Windows.Controls.Image)(target));
            return;
            case 3:
            this.Prezime = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.Ime = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.ImeRoditelja = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.DatumRodjenja = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.MestoOpstinaDrzava = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.Prebivaliste = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.Jmbg = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.Pol = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.mobilnost = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.szr = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.Izdaje = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.BrojDokumenta = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.DatumIzadavanja = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.VaziDo = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.placanje = ((System.Windows.Controls.TextBox)(target));
            return;
            case 18:
            this.sml = ((System.Windows.Controls.Label)(target));
            return;
            case 19:
            this.smestaj = ((System.Windows.Controls.TextBox)(target));
            return;
            case 20:
            this.cenaSmestaja = ((System.Windows.Controls.Label)(target));
            return;
            case 21:
            this.hobi = ((System.Windows.Controls.TextBox)(target));
            return;
            case 22:
            this.ishrana = ((System.Windows.Controls.TextBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

