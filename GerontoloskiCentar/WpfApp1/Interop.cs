﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.Data;

namespace WpfApp1
{
    public class Interop : IDisposable
    {
        bool disposed = false;
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);

        public void Startup(int apiVersion = 3)
        {
            if (apiVersion != 3)
                throw new InvalidApiVersionException("Trenutno podrzana verzija CelikApi je 3. (Zvanicna dokumentacija)");

            Validate(CelikApiMethods.EidStartup(apiVersion), StaticData.ACTION_FAILED, StaticData.STARTUP);
        }

        public CardData Read()
        {
            PEID_DOCUMENT_DATA d = new PEID_DOCUMENT_DATA();
            PEID_FIXED_PERSONAL_DATA f = new PEID_FIXED_PERSONAL_DATA();
            PEID_VARIABLE_PERSONAL_DATA v = new PEID_VARIABLE_PERSONAL_DATA();
            PEID_PORTRAIT p = new PEID_PORTRAIT();

            int c = 2;

            Validate(CelikApiMethods.EidBeginRead(string.Empty, ref c), StaticData.ACTION_FAILED, StaticData.BEGIN_READ);

            Validate(CelikApiMethods.EidReadDocumentData(ref d), StaticData.ACTION_FAILED, StaticData.READ_DOCUMENT_DATA);
            Validate(CelikApiMethods.EidReadFixedPersonalData(ref f), StaticData.ACTION_FAILED, StaticData.READ_FIXED_DATA);
            Validate(CelikApiMethods.EidReadVariablePersonalData(ref v), StaticData.ACTION_FAILED, StaticData.READ_VARIABLE_DATA);
            Validate(CelikApiMethods.EidReadPortrait(ref p), StaticData.ACTION_FAILED, StaticData.READ_PORTRAIT_DATA);

            Validate(CelikApiMethods.EidEndRead(), StaticData.ACTION_FAILED, StaticData.END_READ);

            return new CardData { DocumentData = d, FixedData = f, VariableData = v, PortraitData = p };
        }

        public void Cleanup()
        {
            Validate(CelikApiMethods.EidCleanup(), StaticData.ACTION_FAILED, StaticData.CLEANUP);
        }

        private void Validate(int result, string msg, string action)
        {
            if (result != 0)
                throw new ReadingFailedException(msg) { FailedAction = action, Result = result };
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                handle.Dispose();
            }

            disposed = true;
        }
    }
}

