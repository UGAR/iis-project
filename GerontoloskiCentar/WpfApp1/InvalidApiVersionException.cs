﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{
    public class InvalidApiVersionException : Exception
    {
        public InvalidApiVersionException(string msg)
            : base(msg) { }
    }
}
