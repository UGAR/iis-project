﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Data
{
    public struct PEID_PORTRAIT
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7700)]
        public byte[] portrait;

        public int portraitSize;

        public PEID_PORTRAIT(byte[] portrait, int portraitSize)
        {
            this.portrait = portrait;
            this.portraitSize = portraitSize;
        }
    }
}
