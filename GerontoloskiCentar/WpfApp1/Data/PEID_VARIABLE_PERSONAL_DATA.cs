﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Data
{
    public struct PEID_VARIABLE_PERSONAL_DATA
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
        public string state;

        public int stateSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 200)]
        public string community;

        public int communitySize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 200)]
        public string place;

        public int placeSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 200)]
        public string street;

        public int streetSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
        public string houseNumber;

        public int houseNumberSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
        public string houseLetter;

        public int houseLetterSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 10)]
        public string entrance;

        public int entranceSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 6)]
        public string floor;

        public int floorSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 12)]
        public string apartmentNumber;

        public int apartmentNumberSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 10)]
        public string addressDate;

        public int addressDateSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 60)]
        public string addressLabel;

        public int addressLabelSize;
    }
}
