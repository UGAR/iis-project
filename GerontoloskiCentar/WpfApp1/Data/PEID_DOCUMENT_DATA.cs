﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Data
{
    public struct PEID_DOCUMENT_DATA
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 10)]
        public string docRegNo;

        public int docRegNoSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 2)]
        public string documentType;

        public int documentTypeSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 12)]
        public string issuingDate;

        public int issuingDateSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 12)]
        public string expiryDate;

        public int expiryDateSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
        public string issuingAuthority;

        public int issuingAuthoritySize;
    }
}
