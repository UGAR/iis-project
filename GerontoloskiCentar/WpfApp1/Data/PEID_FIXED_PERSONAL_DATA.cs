﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Data
{
    public struct PEID_FIXED_PERSONAL_DATA
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 14)]
        public string personalNumber;

        int personalNumberSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 200)]
        public string surname;

        int surnameSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 200)]
        public string givenName;

        int givenNameSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 200)]
        public string parentGivenName;

        int parentGivenNameSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 3)]
        public string sex;

        int sexSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 200)]
        public string placeOfBirth;

        int placeOfBirthSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 200)]
        public string stateOfBirth;

        int stateOfBirthSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 12)]
        public string dateOfBirth;

        int dateOfBirthSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 200)]
        public string communityOfBirth;

        int communityOfBirthSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 200)]
        public string statusOfForeigner;

        int statusOfForeignerSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 200)]
        public string nationalityFull;

        int nationalityFullSize;
    }
}
