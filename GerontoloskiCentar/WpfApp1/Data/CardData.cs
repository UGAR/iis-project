﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Data
{
    public struct CardData
    {
        public PEID_DOCUMENT_DATA DocumentData { get; set; }

        public PEID_FIXED_PERSONAL_DATA FixedData { get; set; }

        public PEID_VARIABLE_PERSONAL_DATA VariableData { get; set; }

        public PEID_PORTRAIT PortraitData { get; set; }
    }
}
