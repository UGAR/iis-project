﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace WpfApp1.Data
{
    public class Portrait
    {
        public Bitmap PortraitImg { get; private set; }

        public Portrait(PEID_PORTRAIT nativeData)
        {
            using (var stream = new MemoryStream(nativeData.portrait, 0, nativeData.portraitSize))
            using (var image = Image.FromStream(stream, false, true))
            {
                PortraitImg = new Bitmap(image);
            }

        }
    }
}
