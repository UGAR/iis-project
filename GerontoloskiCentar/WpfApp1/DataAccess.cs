﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.Model;

namespace WpfApp1
{
    public class DataAccess
    {
        public void RegistrationUser(string ime, string prezime, string imeRoditelja, string datumRodjenja, string mestoRodjenja, 
            string jmbg, string prebivaliste, string pol, string izdaje, string brojDokumenta, string datumIzdavanja, string vaziDo, byte[] pom, int pomSize,
             string mobilnost, string szr, string placanje, string smestaj, string hobi, string ishrana)
        {
            using (SqlConnection connection = new SqlConnection(Helper.CnnVal("UgarDB")))
            {
                String query = "INSERT INTO dbo.Users (surname, givenName, sex, parentGivenName, dateOfBirth, mestoRodjenja, personalNumber, prebivaliste, issuingAuthority, docRegNo, issuingDate, expiryDate, portrait, portraitSize, mobilnost, szr, placanje, smestaj, hobi, ishrana) VALUES (@surname, @givenName, @sex, @parentGivenName, @dateOfBirth, @mestoRodjenja, @personalNumber, @prebivaliste, @issuingAuthority, @docRegNo, @issuingDate, @expiryDate, @portrait, @portraitSize, @mobilnost, @szr, @placanje, @smestaj, @hobi, @ishrana)";

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@surname", prezime);
                    command.Parameters.AddWithValue("@givenName", ime);
                    command.Parameters.AddWithValue("@sex", pol);
                    command.Parameters.AddWithValue("@parentGivenName", imeRoditelja);
                    command.Parameters.AddWithValue("@dateOfBirth", datumRodjenja);
                    command.Parameters.AddWithValue("@mestoRodjenja", mestoRodjenja);
                    command.Parameters.AddWithValue("@personalNumber", jmbg);
                    command.Parameters.AddWithValue("@prebivaliste", prebivaliste);
                    command.Parameters.AddWithValue("@issuingAuthority", izdaje);
                    command.Parameters.AddWithValue("@docRegNo", brojDokumenta);
                    command.Parameters.AddWithValue("@issuingDate", datumIzdavanja);
                    command.Parameters.AddWithValue("@expiryDate", vaziDo);
                    command.Parameters.AddWithValue("@portrait", pom);
                    command.Parameters.AddWithValue("@portraitSize", pomSize);
                    command.Parameters.AddWithValue("@mobilnost", mobilnost);
                    command.Parameters.AddWithValue("@szr", szr);
                    command.Parameters.AddWithValue("@placanje", placanje);
                    command.Parameters.AddWithValue("@smestaj", smestaj);
                    command.Parameters.AddWithValue("@hobi", hobi);
                    command.Parameters.AddWithValue("@ishrana", ishrana);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    // Check Error
                    if (result < 0)
                        Console.WriteLine("Error inserting data into Database!");
                }
            }
        }

        public List<Users> getAllUsers()
        {

            using (SqlConnection connection = new SqlConnection(Helper.CnnVal("UgarDB")))
            {
                String query = "SELECT * from dbo.Users";

                var output = connection.Query<Users>(query).ToList();
                return output;
            }
        }

        public List<Rooms> getAllRooms()
        {
            using (SqlConnection connection = new SqlConnection(Helper.CnnVal("UgarDB")))
            {
                String query = "SELECT * from dbo.Rooms";

                var output = connection.Query<Rooms>(query).ToList();
                return output;
            }
        }

        public void updateOccupancyRoom(string sm, int smOccupancyRoom)
        {
            using (SqlConnection connection = new SqlConnection(Helper.CnnVal("UgarDB")))
            {
                String query = "UPDATE dbo.Rooms SET occupancyRoom = @occupancyRoom WHERE nameRoom = @sm";

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@sm", sm);
                    command.Parameters.AddWithValue("@occupancyRoom", smOccupancyRoom);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    // Check Error
                    if (result < 0)
                        Console.WriteLine("Error updating data into Database!");
                }
            }
        }

        public void updateUser(string ime, string prezime, string imeRoditelja, string datumRodjenja, string mestoRodjenja,
            string prebivaliste, string pol, string izdaje, string datumIzdavanja, string vaziDo, byte[] pom, int pomSize,
             string mobilnost, string szr, string placanje, string smestaj, string hobi, string ishrana, string jmbg)
        {
            using (SqlConnection connection = new SqlConnection(Helper.CnnVal("UgarDB")))
            {
                String query = "UPDATE dbo.Users SET surname = @surname, givenName = @givenName, sex = @sex, parentGivenName = @parentGivenName, dateOfBirth = @dateOfBirth, mestoRodjenja = @mestoRodjenja, prebivaliste = @prebivaliste, issuingAuthority = @issuingAuthority, issuingDate = @issuingDate, expiryDate = @expiryDate, portrait = @portrait, portraitSize = @portraitSize, mobilnost = @mobilnost, szr = @szr, placanje = @placanje, smestaj = @smestaj, hobi = @hobi, ishrana = @ishrana WHERE personalNumber = @personalNumber";

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@surname", prezime);
                    command.Parameters.AddWithValue("@givenName", ime);
                    command.Parameters.AddWithValue("@sex", pol);
                    command.Parameters.AddWithValue("@parentGivenName", imeRoditelja);
                    command.Parameters.AddWithValue("@dateOfBirth", datumRodjenja);
                    command.Parameters.AddWithValue("@mestoRodjenja", mestoRodjenja);
                    command.Parameters.AddWithValue("@prebivaliste", prebivaliste);
                    command.Parameters.AddWithValue("@issuingAuthority", izdaje);
                    command.Parameters.AddWithValue("@issuingDate", datumIzdavanja);
                    command.Parameters.AddWithValue("@expiryDate", vaziDo);
                    command.Parameters.AddWithValue("@portrait", pom);
                    command.Parameters.AddWithValue("@portraitSize", pomSize);
                    command.Parameters.AddWithValue("@mobilnost", mobilnost);
                    command.Parameters.AddWithValue("@szr", szr);
                    command.Parameters.AddWithValue("@placanje", placanje);
                    command.Parameters.AddWithValue("@smestaj", smestaj);
                    command.Parameters.AddWithValue("@hobi", hobi);
                    command.Parameters.AddWithValue("@ishrana", ishrana);
                    command.Parameters.AddWithValue("@personalNumber", jmbg);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    // Check Error
                    if (result < 0)
                        Console.WriteLine("Error updating data into Database!");
                }
            }
        }

        public void InsertUserInDeletedUsers(Users pom, string selectedValue)
        {
            using (SqlConnection connection = new SqlConnection(Helper.CnnVal("UgarDB")))
            {
                String query = "INSERT INTO dbo.DeletedUsers (givenNameUser, surnameUser, personalNumberUser, reason) VALUES (@givenNameUser, @surnameUser, @personalNumberUser, @reason)";
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@givenNameUser", pom.givenName);
                    command.Parameters.AddWithValue("@surnameUser", pom.surname);
                    command.Parameters.AddWithValue("@personalNumberUser", pom.personalNumber);
                    command.Parameters.AddWithValue("@reason", selectedValue);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    // Check Error
                    if (result < 0)
                        Console.WriteLine("Error inserting data into Database!");
                }
            }
        }

        public void DeleteUser(string personalNumber)
        {
            using (SqlConnection connection = new SqlConnection(Helper.CnnVal("UgarDB")))
            {
                String query = "DELETE FROM dbo.Users WHERE personalNumber = @personalNumber";

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@personalNumber", personalNumber);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    // Check Error
                    if (result < 0)
                        Console.WriteLine("Error deleting data into Database!");
                }
            }
        }

        public void UpdateRoomForUser(Users pom, string selectedValue)
        {
            using (SqlConnection connection = new SqlConnection(Helper.CnnVal("UgarDB")))
            {
                String query = "UPDATE dbo.Users SET smestaj = @smestaj WHERE personalNumber = @personalNumber";

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@smestaj", selectedValue);
                    command.Parameters.AddWithValue("@personalNumber", pom.personalNumber);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    // Check Error
                    if (result < 0)
                        Console.WriteLine("Error updating data into Database!");
                }
            }
        }

        public void RegistrationWorker(string ime, string prezime, string jmbg, string prebivaliste, string brLicne, string selectedValue, string korIme, string lozinka)
        {
            using (SqlConnection connection = new SqlConnection(Helper.CnnVal("UgarDB")))
            {
                String query = "INSERT INTO dbo.Workers (nameWorker, surnameWorker, personalNumberWorker, docRegNoWorker, prebivalisteWorker, placeWorker, username, passwordWorker) VALUES (@nameWorker, @surnameWorker, @personalNumberWorker, @docRegNoWorker, @prebivalisteWorker, @placeWorker, @username, @passwordWorker)";

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@nameWorker", ime);
                    command.Parameters.AddWithValue("@surnameWorker", prezime);
                    command.Parameters.AddWithValue("@personalNumberWorker", jmbg);
                    command.Parameters.AddWithValue("@docRegNoWorker", brLicne);
                    command.Parameters.AddWithValue("@prebivalisteWorker", prebivaliste);
                    command.Parameters.AddWithValue("@placeWorker", selectedValue);
                    command.Parameters.AddWithValue("@username", korIme);
                    command.Parameters.AddWithValue("@passwordWorker", lozinka);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    // Check Error
                    if (result < 0)
                        Console.WriteLine("Error inserting data into Database!");
                }
            }
        }

        public List<Workers> getAllWorkers()
        {
            using (SqlConnection connection = new SqlConnection(Helper.CnnVal("UgarDB")))
            {
                String query = "SELECT * from dbo.Workers";

                var output = connection.Query<Workers>(query).ToList();
                return output;
            }
        }

        public List<Workers> getAllWorkersForListOfView()
        {
            using (SqlConnection connection = new SqlConnection(Helper.CnnVal("UgarDB")))
            {
                String query = "SELECT * from dbo.Workers WHERE placeWorker not like 'admin'";

                var output = connection.Query<Workers>(query).ToList();
                return output;
            }
        }

        public void DeleteWorker(Workers obj)
        {
            using (SqlConnection connection = new SqlConnection(Helper.CnnVal("UgarDB")))
            {
                String query = "DELETE FROM dbo.Workers WHERE personalNumberWorker = @personalNumberWorker";

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@personalNumberWorker", obj.personalNumberWorker);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    // Check Error
                    if (result < 0)
                        Console.WriteLine("Error deleting data into Database!");
                }
            }
        }

        public void RegistrationRoom(string naziv, string tip, string cena, string kapacitet)
        {
            using (SqlConnection connection = new SqlConnection(Helper.CnnVal("UgarDB")))
            {
                String query = "INSERT INTO dbo.Rooms (nameRoom, typeRoom, priceRoom, capacityRoom, occupancyRoom) VALUES (@nameRoom, @typeRoom, @priceRoom, @capacityRoom, @occupancyRoom)";

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@nameRoom", naziv);
                    command.Parameters.AddWithValue("@typeRoom", tip);
                    command.Parameters.AddWithValue("@priceRoom", cena);
                    command.Parameters.AddWithValue("@capacityRoom", kapacitet);
                    command.Parameters.AddWithValue("@occupancyRoom", 0);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    // Check Error
                    if (result < 0)
                        Console.WriteLine("Error inserting data into Database!");
                }
            }
        }

        public void DeleteRoom(Rooms obj)
        {
            using (SqlConnection connection = new SqlConnection(Helper.CnnVal("UgarDB")))
            {
                String query = "DELETE FROM dbo.Rooms WHERE nameRoom = @nameRoom";

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@nameRoom", obj.nameRoom);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    // Check Error
                    if (result < 0)
                        Console.WriteLine("Error deleting data into Database!");
                }
            }
        }

        public void updateWorker(Workers pom, string ime, string prezime, string prebivaliste, string selectedValue, string korIme, string lozinka)
        {
            using (SqlConnection connection = new SqlConnection(Helper.CnnVal("UgarDB")))
            {
                String query = "UPDATE dbo.Workers SET nameWorker = @nameWorker, surnameWorker = @surnameWorker, prebivalisteWorker = @prebivalisteWorker, placeWorker = @placeWorker, username = @username, passwordWorker = @passwordWorker WHERE personalNumberWorker = @personalNumberWorker";

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@nameWorker", ime);
                    command.Parameters.AddWithValue("@surnameWorker", prezime);
                    command.Parameters.AddWithValue("@personalNumberWorker", pom.personalNumberWorker);
                    command.Parameters.AddWithValue("@prebivalisteWorker", prebivaliste);
                    command.Parameters.AddWithValue("@placeWorker", selectedValue);
                    command.Parameters.AddWithValue("@username", korIme);
                    command.Parameters.AddWithValue("@passwordWorker", lozinka);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    // Check Error
                    if (result < 0)
                        Console.WriteLine("Error updating data into Database!");
                }
            }
        }

        public void updateRoom(Rooms pom, string naziv, string selectedValue, string cena, string kapacitet)
        {
            using (SqlConnection connection = new SqlConnection(Helper.CnnVal("UgarDB")))
            {
                String query = "UPDATE dbo.Rooms SET nameRoom = @nameRoom1, typeRoom = @typeRoom, priceRoom = @priceRoom, capacityRoom = @capacityRoom WHERE nameRoom = @nameRoom";

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@nameRoom1", naziv);
                    command.Parameters.AddWithValue("@nameRoom", pom.nameRoom);
                    command.Parameters.AddWithValue("@typeRoom", selectedValue);
                    command.Parameters.AddWithValue("@priceRoom", cena);
                    command.Parameters.AddWithValue("@capacityRoom", kapacitet);

                    connection.Open();
                    int result = command.ExecuteNonQuery();

                    // Check Error
                    if (result < 0)
                        Console.WriteLine("Error updating data into Database!");
                }
            }
        }
    }
}
