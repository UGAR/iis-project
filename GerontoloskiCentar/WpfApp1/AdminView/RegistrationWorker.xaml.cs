﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1.Data;
using WpfApp1.Model;

namespace WpfApp1.AdminView
{
    /// <summary>
    /// Interaction logic for RegistrationWorker.xaml
    /// </summary>
    public partial class RegistrationWorker : Window
    {
        public List<Workers> lista = new List<Workers>();

        public RegistrationWorker()
        {
            InitializeComponent();
        }

        private void UcitajPodatke_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (Interop interop = new Interop())
                {
                    interop.Startup();
                    CardData cardData = interop.Read();
                    Prezime.Text = cardData.FixedData.surname;
                    Ime.Text = cardData.FixedData.givenName;
                    Jmbg.Text = cardData.FixedData.personalNumber;
                    Prebivaliste.Text = cardData.VariableData.community + ", " + cardData.VariableData.place + ", " +
                        cardData.VariableData.street + " " + cardData.VariableData.houseNumber + "/" + cardData.VariableData.apartmentNumber;
                    BrojDokumenta.Text = cardData.DocumentData.docRegNo;
                    interop.Cleanup();
                }
            }
            catch (InvalidApiVersionException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (ReadingFailedException ex)
            {
                Console.WriteLine("Greska prilikom citanja sa kartice. Poziv {0} nije uspeo, kod greske: {1}", ex.FailedAction, ex.Result);
            }
        }

        private void Registration_Click(object sender, RoutedEventArgs e)
        {
            if (Validation() && UserValidation() && PasswordValidation())
            {
                DataAccess dt = new DataAccess();
                dt.RegistrationWorker(Ime.Text, Prezime.Text, Jmbg.Text, Prebivaliste.Text, BrojDokumenta.Text, ((ComboBoxItem)radnoMestoComboBox.SelectedValue).Content.ToString(), korIme.Text, lozinka.Password.ToString());
                ListOfWorkers l = new ListOfWorkers();
                l.Show();
                this.Close();
            }
        }

        private bool PasswordValidation()
        {
            bool result = true;

            if(lozinka.Password.ToString() != ponoviLozinku.Password.ToString())
            {
                result = false;
                MessageBox.Show("Lozinke se ne poklapaju!");
            }

            return result;
        }

        private bool UserValidation()
        {
            bool result = true;
            DataAccess dt = new DataAccess();
            lista = dt.getAllWorkers();
            foreach(Workers w in lista)
            {
                if(w.personalNumberWorker == Jmbg.Text && w.DocRegNoWorker == BrojDokumenta.Text)
                {
                    result = false;
                    MessageBox.Show("Radnik već postoji!");
                }

                if(w.username == korIme.Text)
                {
                    result = false;
                    MessageBox.Show("Korisničko ime već postoji!");
                }
            }

            return result;
        }

        private bool Validation()
        {
            bool result = true;

            if (Ime.Text == "")
            {
                result = false;
                Ime.BorderBrush = Brushes.Red;
            }
            else
            {
                Ime.BorderBrush = Brushes.Gray;
            }

            if (Prezime.Text == "")
            {
                result = false;
                Prezime.BorderBrush = Brushes.Red;
            }
            else
            {
                Prezime.BorderBrush = Brushes.Gray;
            }

            if (Ime.Text == "")
            {
                result = false;
                Ime.BorderBrush = Brushes.Red;
            }
            else
            {
                Ime.BorderBrush = Brushes.Gray;
            }

            if (Jmbg.Text == "")
            {
                result = false;
                Jmbg.BorderBrush = Brushes.Red;
            }
            else
            {
                Jmbg.BorderBrush = Brushes.Gray;
            }

            if (Prebivaliste.Text == "")
            {
                result = false;
                Prebivaliste.BorderBrush = Brushes.Red;
            }
            else
            {
                Prebivaliste.BorderBrush = Brushes.Gray;
            }

            if (BrojDokumenta.Text == "")
            {
                result = false;
                BrojDokumenta.BorderBrush = Brushes.Red;
            }
            else
            {
                BrojDokumenta.BorderBrush = Brushes.Gray;
            }

            if (radnoMestoComboBox.SelectedValue == null)
            {
                result = false;
                radnoMesto.Foreground = Brushes.Red;
            }
            else
            {
                radnoMesto.Foreground = Brushes.Black;
            }

            if (korIme.Text == "")
            {
                result = false;
                korIme.BorderBrush = Brushes.Red;
            }
            else
            {
                korIme.BorderBrush = Brushes.Gray;
            }

            if (lozinka.Password.ToString() == "")
            {
                result = false;
                lozinka.BorderBrush = Brushes.Red;
            }
            else
            {
                lozinka.BorderBrush = Brushes.Gray;
            }

            if (ponoviLozinku.Password.ToString() == "")
            {
                result = false;
                ponoviLozinku.BorderBrush = Brushes.Red;
            }
            else
            {
                ponoviLozinku.BorderBrush = Brushes.Gray;
            }

            return result;
        }
    }
}
