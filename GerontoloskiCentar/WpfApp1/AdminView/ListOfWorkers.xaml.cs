﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1.Model;

namespace WpfApp1.AdminView
{
    /// <summary>
    /// Interaction logic for ListOfWorkers.xaml
    /// </summary>
    public partial class ListOfWorkers : Window
    {
        public List<Workers> lista = new List<Workers>();
        public List<Workers> filter = new List<Workers>();

        public ListOfWorkers()
        {
            InitializeComponent();
            DataAccess dt = new DataAccess();
            lista = dt.getAllWorkersForListOfView();
            WorkersGrid.ItemsSource = lista;
        }

        private void Pretrazi_Click(object sender, RoutedEventArgs e)
        {
            filter.Clear();
            WorkersGrid.Items.Refresh();

            foreach (Workers w in lista)
            {
                if(ime.Text == "" && prezime.Text == "" && pozicija.Text == "")
                {
                    WorkersGrid.ItemsSource = lista;
                    WorkersGrid.Items.Refresh();
                }
                else
                {
                    if (ime.Text.ToLower() == w.nameWorker.ToLower() && prezime.Text.ToLower() == w.surnameWorker.ToLower()
                    && pozicija.Text.ToLower() == w.placeWorker.ToLower())
                    {
                        filter.Add(w);
                        WorkersGrid.ItemsSource = filter;
                        WorkersGrid.Items.Refresh();
                    }
                }
            }
        }

        private void Delete(object sender, RoutedEventArgs e)
        {
            Workers obj = ((FrameworkElement)sender).DataContext as Workers;
            DataAccess dt = new DataAccess();
            dt.DeleteWorker(obj);
            ListOfWorkers l = new ListOfWorkers();
            this.Close();
            l.Show();
        }

        private void UpdateWorker(object sender, RoutedEventArgs e)
        {
            Workers obj = ((FrameworkElement)sender).DataContext as Workers;
            UpdateWorker u = new UpdateWorker(obj);
            u.Show();
            this.Close();
        }
    }
}
