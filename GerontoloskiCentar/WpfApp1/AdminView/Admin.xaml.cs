﻿using projekat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1.Model;

namespace WpfApp1.AdminView
{
    /// <summary>
    /// Interaction logic for Admin.xaml
    /// </summary>
    public partial class Admin : Window
    {
        public Admin()
        {
            InitializeComponent();
        }

        private void registrationNewUser_Click(object sender, RoutedEventArgs e)
        {
            RegistrationWorker r = new RegistrationWorker();
            r.Show();
            int counter = 0;
            for (int intCounter = App.Current.Windows.Count - 1; intCounter > -1; intCounter--)
            {
                if (App.Current.Windows[intCounter].Name == "rw")
                {
                    counter++;
                    if (counter > 1)
                    {
                        App.Current.Windows[intCounter].Visibility = System.Windows.Visibility.Hidden;
                    }
                }
                if (App.Current.Windows[intCounter].Name != "Admin1" && App.Current.Windows[intCounter].Name != "rw")
                {
                    App.Current.Windows[intCounter].Visibility = System.Windows.Visibility.Hidden;
                }
            }
        }

        private void listOfWorkers_Click(object sender, RoutedEventArgs e)
        {
            ListOfWorkers l = new ListOfWorkers();
            l.Show();
            int counter = 0;
            for (int intCounter = App.Current.Windows.Count - 1; intCounter > -1; intCounter--)
            {
                if (App.Current.Windows[intCounter].Name == "low")
                {
                    counter++;
                    if (counter > 1)
                    {
                        App.Current.Windows[intCounter].Visibility = System.Windows.Visibility.Hidden;
                    }
                }
                if (App.Current.Windows[intCounter].Name != "Admin1" && App.Current.Windows[intCounter].Name != "low")
                {
                    App.Current.Windows[intCounter].Visibility = System.Windows.Visibility.Hidden;
                }
            }
        }

        private void ViewRooms_Click(object sender, RoutedEventArgs e)
        {
            ViewRooms r = new ViewRooms();
            r.Show();
            int counter = 0;
            for (int intCounter = App.Current.Windows.Count - 1; intCounter > -1; intCounter--)
            {
                if (App.Current.Windows[intCounter].Name == "vr")
                {
                    counter++;
                    if (counter > 1)
                    {
                        App.Current.Windows[intCounter].Visibility = System.Windows.Visibility.Hidden;
                    }
                }
                if (App.Current.Windows[intCounter].Name != "Admin1" && App.Current.Windows[intCounter].Name != "vr")
                {
                    App.Current.Windows[intCounter].Visibility = System.Windows.Visibility.Hidden;
                }
            }
        }

        private void registrationRoom_Click(object sender, RoutedEventArgs e)
        {
            RegistrationRoom r = new RegistrationRoom();
            r.Show();
            int counter = 0;
            for (int intCounter = App.Current.Windows.Count - 1; intCounter > -1; intCounter--)
            {
                if (App.Current.Windows[intCounter].Name == "rr")
                {
                    counter++;
                    if (counter > 1)
                    {
                        App.Current.Windows[intCounter].Visibility = System.Windows.Visibility.Hidden;
                    }
                }
                if (App.Current.Windows[intCounter].Name != "Admin1" && App.Current.Windows[intCounter].Name != "rr")
                {
                    App.Current.Windows[intCounter].Visibility = System.Windows.Visibility.Hidden;
                }
            }
        }

        private void LogOut_Click(object sender, RoutedEventArgs e)
        {
            MainWindow m = new MainWindow();
            int intCounter = App.Current.Windows.Count - 1;
            for (int j = 0; j < intCounter; j++)
            {
                App.Current.Windows[j].Hide();
            }
            m.ShowDialog();
        }

        private void CloseAllWindows()
        {
            for (int intCounter = App.Current.Windows.Count - 1; intCounter >= 0; intCounter--)
                App.Current.Windows[intCounter].Hide();
        }

        private void Settings_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
