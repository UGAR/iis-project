﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1.Model;

namespace WpfApp1.AdminView
{
    /// <summary>
    /// Interaction logic for Rooms.xaml
    /// </summary>
    public partial class ViewRooms : Window
    {
        private List<Rooms> lista = new List<Rooms>();
        private List<Rooms> filter = new List<Rooms>();

        public ViewRooms()
        {
            InitializeComponent();
            DataAccess dt = new DataAccess();
            lista = dt.getAllRooms();
            RoomsGrid.ItemsSource = lista;
        }

        private void Pretrazi_Click(object sender, RoutedEventArgs e)
        {
            filter.Clear();
            RoomsGrid.Items.Refresh();

            foreach (Rooms r in lista)
            {
                if ("" == naziv.Text && "" == tip.Text && "" == cena.Text)
                {
                    RoomsGrid.ItemsSource = lista;
                    RoomsGrid.Items.Refresh();
                }
                else
                {
                    if (r.nameRoom.ToLower() == naziv.Text.ToLower() && r.typeRoom.ToLower() == tip.Text.ToLower()
                    && r.priceRoom.ToLower() == cena.Text.ToLower())
                    {
                        filter.Add(r);
                        RoomsGrid.ItemsSource = filter;
                        RoomsGrid.Items.Refresh();
                    }
                }
            }
        }

        private void Delete(object sender, RoutedEventArgs e)
        {
            Rooms obj = ((FrameworkElement)sender).DataContext as Rooms;
            DataAccess dt = new DataAccess();
            dt.DeleteRoom(obj);
            ViewRooms v = new ViewRooms();
            v.Show();
            this.Close();
        }

        private void Update(object sender, RoutedEventArgs e)
        {
            Rooms obj = ((FrameworkElement)sender).DataContext as Rooms;
            UpdateRoom u = new UpdateRoom(obj);
            u.Show();
            this.Close();
        }
    }
}
