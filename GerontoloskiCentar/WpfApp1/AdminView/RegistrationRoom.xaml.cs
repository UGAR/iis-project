﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1.Model;

namespace WpfApp1.AdminView
{
    /// <summary>
    /// Interaction logic for RegistrationRoom.xaml
    /// </summary>
    public partial class RegistrationRoom : Window
    {
        public RegistrationRoom()
        {
            InitializeComponent();
        }

        private void Registration_Click(object sender, RoutedEventArgs e)
        {
            if (Validation() && ValidationRoom())
            {
                DataAccess dt = new DataAccess();
                dt.RegistrationRoom(naziv.Text, ((ComboBoxItem)tip.SelectedValue).Content.ToString(), cena.Text, kapacitet.Text);
                ViewRooms v = new ViewRooms();
                v.Show();
                this.Close();
            }
        }

        private bool ValidationRoom()
        {
            bool result = true;
            DataAccess dt = new DataAccess();
            List<Rooms> lista = new List<Rooms>();
            lista = dt.getAllRooms();

            foreach(Rooms r in lista)
            {
                if(r.nameRoom == naziv.Text)
                {
                    result = false;
                    MessageBox.Show("Naziv smeštaja već postoji!");
                }
            }

            return result;
        }

        private bool Validation()
        {
            bool result = true;

            if (naziv.Text == "")
            {
                result = false;
                naziv.BorderBrush = Brushes.Red;
            }
            else
            {
                naziv.BorderBrush = Brushes.Gray;
            }

            if (tip.SelectedValue == null)
            {
                result = false;
                tipGreska.Foreground = Brushes.Red;
            }
            else
            {
                tipGreska.Foreground = Brushes.Black;
            }

            if (cena.Text == "")
            {
                result = false;
                cena.BorderBrush = Brushes.Red;
            }
            else
            {
                cena.BorderBrush = Brushes.Gray;
            }

            if(!Double.TryParse(cena.Text, out double n))
            {
                result = false;
                cena.BorderBrush = Brushes.Red;
            }
            else
            {
                cena.BorderBrush = Brushes.Gray;
            }

            if (kapacitet.Text == "")
            {
                result = false;
                kapacitet.BorderBrush = Brushes.Red;
            }
            else
            {
                kapacitet.BorderBrush = Brushes.Gray;
            }

            if (!Double.TryParse(kapacitet.Text, out double s))
            {
                result = false;
                kapacitet.BorderBrush = Brushes.Red;
            }
            else
            {
                kapacitet.BorderBrush = Brushes.Gray;
            }

            return result;
        }
    }
}
