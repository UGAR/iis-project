﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1.Model;

namespace WpfApp1.AdminView
{
    /// <summary>
    /// Interaction logic for UpdateRoom.xaml
    /// </summary>
    public partial class UpdateRoom : Window
    {
        public Rooms pom = new Rooms();
        public List<Rooms> lista = new List<Rooms>();

        public UpdateRoom(Rooms obj)
        {
            InitializeComponent();
            popuniPolja(obj);
            DataAccess dt = new DataAccess();
            lista = dt.getAllRooms();
            pom = obj;
        }

        private void popuniPolja(Rooms room)
        {
            naziv.Text = room.nameRoom;
            tip.SelectedValue = room.typeRoom;
            cena.Text = room.priceRoom;
            kapacitet.Text = room.capacityRoom.ToString();
        }

        private void Update_Click(object sender, RoutedEventArgs e)
        {
            if (Validation())
            {
                DataAccess dt = new DataAccess();
                dt.updateRoom(pom, naziv.Text, (string)tip.SelectedValue, cena.Text, kapacitet.Text);
                ViewRooms v = new ViewRooms();
                v.Show();
                this.Close();
            }
        }

        private bool Validation()
        {
            bool result = true;

            foreach (Rooms r in lista)
            {
                if (naziv.Text == r.nameRoom && naziv.Text != pom.nameRoom || naziv.Text == "")
                {
                    result = false;
                    naziv.BorderBrush = Brushes.Red;
                    break;
                }
                else
                {
                    naziv.BorderBrush = Brushes.Gray;
                }
            }

            /*if (naziv.Text == "")
            {
                result = false;
                naziv.BorderBrush = Brushes.Red;
            }
            else
            {
                naziv.BorderBrush = Brushes.Gray;
            }*/

            if (tip.SelectedValue == null)
            {
                result = false;
                tipGreska.Foreground = Brushes.Red;
            }
            else
            {
                tipGreska.Foreground = Brushes.Black;
            }

            if (cena.Text == "")
            {
                result = false;
                cena.BorderBrush = Brushes.Red;
            }
            else
            {
                cena.BorderBrush = Brushes.Gray;
            }

            if (!Double.TryParse(cena.Text, out double n))
            {
                result = false;
                cena.BorderBrush = Brushes.Red;
            }
            else
            {
                cena.BorderBrush = Brushes.Gray;
            }

            if (kapacitet.Text == "")
            {
                result = false;
                kapacitet.BorderBrush = Brushes.Red;
            }
            else
            {
                kapacitet.BorderBrush = Brushes.Gray;
            }

            if (!Double.TryParse(kapacitet.Text, out double s))
            {
                result = false;
                kapacitet.BorderBrush = Brushes.Red;
            }
            else
            {
                kapacitet.BorderBrush = Brushes.Gray;
            }

            return result;
        }
    }
}
