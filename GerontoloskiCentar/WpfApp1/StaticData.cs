﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{
    public class StaticData
    {
        public static readonly string ACTION_FAILED = "Akcija nije uspela.";

        public const string STARTUP = "EidStartup";
        public const string CLEANUP = "EidCleanup";
        public const string BEGIN_READ = "EidBeginRead";
        public const string END_READ = "EidEndRead";
        public const string READ_DOCUMENT_DATA = "EidReadDocumentData";
        public const string READ_VARIABLE_DATA = "EidReadVariablePersonalData";
        public const string READ_FIXED_DATA = "EidReadFixedPersonalData";
        public const string READ_PORTRAIT_DATA = "EidReadPortrait";
    }
}
