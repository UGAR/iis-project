﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1.Model;

namespace WpfApp1.View
{
    /// <summary>
    /// Interaction logic for DeleteUser.xaml
    /// </summary>
    public partial class DeleteUser : Window
    {
        public Users pom = new Users();
        public List<Rooms> lista = new List<Rooms>();
        public int occupacity = 0;

        public DeleteUser(Users user)
        {
            InitializeComponent();
            DataAccess dt = new DataAccess();
            lista = dt.getAllRooms();
            foreach (Rooms r in lista)
            {
                if(r.nameRoom == user.smestaj)
                {
                    occupacity = r.occupancyRoom;
                }
            }
            pom = user;
            ime.Text = user.givenName;
            prezime.Text = user.surname;
            jmbg.Text = user.personalNumber;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Validation())
            {
                DataAccess dt = new DataAccess();
                dt.DeleteUser(pom.personalNumber);
                dt.InsertUserInDeletedUsers(pom, ((ComboBoxItem)razlogComboBox.SelectedValue).Content.ToString());
                dt.updateOccupancyRoom(pom.smestaj, occupacity - 1);
                ListOfUsers l = new ListOfUsers();
                l.Show();
                this.Close();
            }
        }

        private bool Validation()
        {
            bool result = true;

            if (razlogComboBox.SelectedValue == null)
            {
                result = false;
                razlogGreska.Foreground = Brushes.Red;
            }
            else
            {
                razlogGreska.Foreground = Brushes.Gray;
            }

            return result;
        }
    }
}
