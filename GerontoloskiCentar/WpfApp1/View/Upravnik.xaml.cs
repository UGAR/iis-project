﻿using projekat;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1.Data;

namespace WpfApp1.View
{
    /// <summary>
    /// Interaction logic for Upravnik.xaml
    /// </summary>
    public partial class Upravnik : Window
    {
        public Upravnik()
        {
            InitializeComponent();
        }

        BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (Interop interop = new Interop())
                {
                    interop.Startup();
                    CardData cardData = interop.Read();
                    /* JMBG.Content = cardData.FixedData.givenName;*/
                    Portrait p = new Portrait(cardData.PortraitData);
                    /* slika.Source = BitmapToImageSource(p.PortraitImg);*/
                    interop.Cleanup();
                }
            }
            catch (InvalidApiVersionException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (ReadingFailedException ex)
            {
                Console.WriteLine("Greska prilikom citanja sa kartice. Poziv {0} nije uspeo, kod greske: {1}", ex.FailedAction, ex.Result);
            }
        }

        private void registrationNewUser_Click(object sender, RoutedEventArgs e)
        {
            RegistrationUser r = new RegistrationUser();
            r.Show();
            int counter = 0;
            for (int intCounter = App.Current.Windows.Count - 1; intCounter > -1; intCounter--)
            {
                if (App.Current.Windows[intCounter].Name == "ru")
                {
                    counter++;
                    if (counter > 1)
                    {
                        App.Current.Windows[intCounter].Visibility = System.Windows.Visibility.Hidden;
                    }
                }
                if (App.Current.Windows[intCounter].Name != "Upravnik1" && App.Current.Windows[intCounter].Name != "ru")
                {
                    App.Current.Windows[intCounter].Visibility = System.Windows.Visibility.Hidden;
                }
            }
        }

        private void listUsers_Click(object sender, RoutedEventArgs e)
        {
            ListOfUsers l = new ListOfUsers();
            l.Show();
            int counter = 0;
            for (int intCounter = App.Current.Windows.Count - 1; intCounter > -1; intCounter--)
            {
                if (App.Current.Windows[intCounter].Name == "lou")
                {
                    counter++;
                    if (counter > 1)
                    {
                        App.Current.Windows[intCounter].Visibility = System.Windows.Visibility.Hidden;
                    }
                }
                if (App.Current.Windows[intCounter].Name != "Upravnik1" && App.Current.Windows[intCounter].Name != "lou")
                    App.Current.Windows[intCounter].Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void viewRooms_Click(object sender, RoutedEventArgs e)
        {
            ListOfRooms l = new ListOfRooms();
            l.Show();
            int counter = 0;
            for (int intCounter = App.Current.Windows.Count - 1; intCounter > -1; intCounter--)
            {
                if (App.Current.Windows[intCounter].Name == "lor")
                {
                    counter++;
                    if (counter > 1)
                    {
                        App.Current.Windows[intCounter].Visibility = System.Windows.Visibility.Hidden;
                    }
                }
                if (App.Current.Windows[intCounter].Name != "Upravnik1" && App.Current.Windows[intCounter].Name != "lor")
                    App.Current.Windows[intCounter].Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void changeRoom_Click(object sender, RoutedEventArgs e)
        {
            ChangeRoom c = new ChangeRoom();
            c.Show();
            int counter = 0;
            for (int intCounter = App.Current.Windows.Count - 1; intCounter > -1; intCounter--)
            {
                if (App.Current.Windows[intCounter].Name == "c")
                {
                    counter++;
                    if (counter > 1)
                    {
                        App.Current.Windows[intCounter].Visibility = System.Windows.Visibility.Hidden;
                    }
                }
                if (App.Current.Windows[intCounter].Name != "Upravnik1" && App.Current.Windows[intCounter].Name != "c")
                    App.Current.Windows[intCounter].Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void LogOut_Click(object sender, RoutedEventArgs e)
        {
            MainWindow m = new MainWindow();
            int intCounter = App.Current.Windows.Count - 1;
            for (int j = 0; j < intCounter; j++)
            {
                App.Current.Windows[j].Hide();
            }
            m.ShowDialog();
        }

        private void CloseAllWindows()
        {
            for (int intCounter = App.Current.Windows.Count - 1; intCounter >= 0; intCounter--)
                App.Current.Windows[intCounter].Hide();
        }

        private void Settings_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
