﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1.Model;

namespace WpfApp1.View
{
    /// <summary>
    /// Interaction logic for RoomDetails.xaml
    /// </summary>
    public partial class RoomDetails : Window
    {
        public static List<Users> listOfUsers = new List<Users>();
        public static List<Users> pom = new List<Users>();

        public RoomDetails(Rooms room)
        {
            InitializeComponent();
            getUsers(room);
            listaKorisnikaSmestaja.ItemsSource = pom;
            nameRoom.Content = room.nameRoom;
            typeRoom.Content = room.typeRoom;
            occupancyRoom.Content = room.occupancyRoom.ToString() + "/" + room.capacityRoom.ToString();
        }

        private void getUsers(Rooms room)
        {
            DataAccess dt = new DataAccess();
            listOfUsers = dt.getAllUsers();
            pom.Clear();

            foreach (Users u in listOfUsers)
            {
                if (room.nameRoom == u.smestaj)
                {
                    pom.Add(u);
                }
            }
        }
    }
}
