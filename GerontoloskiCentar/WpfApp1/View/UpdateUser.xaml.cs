﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1.Data;
using WpfApp1.Model;
using Brushes = System.Windows.Media.Brushes;

namespace WpfApp1.View
{
    /// <summary>
    /// Interaction logic for UpdateUser.xaml
    /// </summary>
    public partial class UpdateUser : Window
    {
        public List<Rooms> listAllRooms = new List<Rooms>();
        public List<Users> listAllUsers = new List<Users>();
        public List<string> listAllUsersPom = new List<string>();
        public byte[] pom;
        public int pomSize;
        public Users userPom = new Users();

        BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        public UpdateUser(Users user)
        {
            InitializeComponent();
            popuniPolja(user);
            userPom = user;
            DataAccess dt = new DataAccess();
            listAllRooms = dt.getAllRooms();
            foreach (Rooms r in listAllRooms)
            {
                if (r.occupancyRoom < r.capacityRoom || user.smestaj == r.nameRoom)
                    smestajComboBox.Items.Add(r.nameRoom);
            }
        }

        private void popuniPolja(Users user)
        {
            PEID_PORTRAIT pPom = new PEID_PORTRAIT(user.portrait, user.portraitSize);
            Portrait p = new Portrait(pPom);
            pom = user.portrait;
            pomSize = user.portraitSize;
            PortraitImage.Source = BitmapToImageSource(p.PortraitImg);
            Prezime.Text = user.surname;
            Ime.Text = user.givenName;
            ImeRoditelja.Text = user.parentGivenName;
            DatumRodjenja.Text = user.dateOfBirth;
            MestoOpstinaDrzava.Text = user.mestoRodjenja;
            Jmbg.Text = user.personalNumber;
            Prebivaliste.Text = user.prebivaliste;
            Pol.Text = user.sex;
            Izdaje.Text = user.issuingAuthority;
            BrojDokumenta.Text = user.docRegNo;
            DatumIzadavanja.Text = user.issuingDate;
            VaziDo.Text = user.expiryDate;
            mobilnostComboBox.SelectedValue = user.mobilnost;
            mobilnostComboBox.Items.Refresh();
            szrComboBox.SelectedValue = user.szr;
            szrComboBox.Items.Refresh();
            placanjeComboBox.SelectedValue = user.placanje;
            smestajComboBox.SelectedValue = user.smestaj;
            hobiComboBox.SelectedValue = user.hobi;
            ishranaComboBox.SelectedValue = user.ishrana;
            foreach(Rooms r in listAllRooms)
            {
                if (user.smestaj == r.nameRoom)
                    cenaSmestaja.Content = r.priceRoom;
            }
        }

        private void Update_Click(object sender, RoutedEventArgs e)
        {
            if (CellsValidation())
            {
                DataAccess dt = new DataAccess();
                string m = ((ComboBoxItem)mobilnostComboBox.SelectedItem).Content.ToString();
                string s = ((ComboBoxItem)szrComboBox.SelectedItem).Content.ToString();
                string p = ((ComboBoxItem)placanjeComboBox.SelectedItem).Content.ToString();
                string sm = (string)smestajComboBox.SelectedItem;
                int smOccupancyRoom = 0;
                foreach (Rooms rooms in listAllRooms)
                {
                    if (rooms.nameRoom == sm)
                    {
                        smOccupancyRoom = rooms.occupancyRoom + 1;
                    }
                }
                string h = "";
                string i = "";
                if (hobiComboBox.SelectedItem != null)
                    h = ((ComboBoxItem)hobiComboBox.SelectedItem).Content.ToString();
                if (ishranaComboBox.SelectedItem != null)
                    i = ((ComboBoxItem)ishranaComboBox.SelectedItem).Content.ToString();
                dt.updateUser(Ime.Text.ToString(), Prezime.Text.ToString(), ImeRoditelja.Text.ToString(), DatumRodjenja.Text.ToString(),
                    MestoOpstinaDrzava.Text.ToString(), Prebivaliste.Text.ToString(), Pol.Text.ToString(),
                    Izdaje.Text.ToString(), DatumIzadavanja.Text.ToString(), VaziDo.Text.ToString(), pom, pomSize,
                    m, s, p, sm, h, i, Jmbg.Text);
                if((string)smestajComboBox.SelectedValue != userPom.smestaj)
                    dt.updateOccupancyRoom(sm, smOccupancyRoom);
                ListOfUsers l = new ListOfUsers();
                l.Show();
                this.Close();
            }
        }

        private bool CellsValidation()
        {
            bool result = true;

            if (PortraitImage.Source == null)
            {
                result = false;
                imageP.BorderBrush = Brushes.Red;
            }
            else
            {
                imageP.BorderBrush = Brushes.Gray;
            }

            if (Prezime.Text == "")
            {
                result = false;
                Prezime.BorderBrush = Brushes.Red;
            }
            else
            {
                Prezime.BorderBrush = Brushes.Gray;
            }

            if (Ime.Text == "")
            {
                result = false;
                Ime.BorderBrush = Brushes.Red;
            }
            else
            {
                Ime.BorderBrush = Brushes.Gray;
            }

            if (ImeRoditelja.Text == "")
            {
                result = false;
                ImeRoditelja.BorderBrush = Brushes.Red;
            }
            else
            {
                ImeRoditelja.BorderBrush = Brushes.Gray;
            }

            if (DatumRodjenja.Text == "")
            {
                result = false;
                DatumRodjenja.BorderBrush = Brushes.Red;
            }
            else
            {
                DatumRodjenja.BorderBrush = Brushes.Gray;
            }

            if (MestoOpstinaDrzava.Text == "")
            {
                result = false;
                MestoOpstinaDrzava.BorderBrush = Brushes.Red;
            }
            else
            {
                MestoOpstinaDrzava.BorderBrush = Brushes.Gray;
            }

            if (Jmbg.Text == "")
            {
                result = false;
                Jmbg.BorderBrush = Brushes.Red;
            }
            else
            {
                Jmbg.BorderBrush = Brushes.Gray;
            }

            if (Prebivaliste.Text == "")
            {
                result = false;
                Prebivaliste.BorderBrush = Brushes.Red;
            }
            else
            {
                Prebivaliste.BorderBrush = Brushes.Gray;
            }

            if (Pol.Text == "")
            {
                result = false;
                Pol.BorderBrush = Brushes.Red;
            }
            else
            {
                Pol.BorderBrush = Brushes.Gray;
            }

            if (Izdaje.Text == "")
            {
                result = false;
                Izdaje.BorderBrush = Brushes.Red;
            }
            else
            {
                Izdaje.BorderBrush = Brushes.Gray;
            }

            if (BrojDokumenta.Text == "")
            {
                result = false;
                BrojDokumenta.BorderBrush = Brushes.Red;
            }
            else
            {
                BrojDokumenta.BorderBrush = Brushes.Gray;
            }

            if (DatumIzadavanja.Text == "")
            {
                result = false;
                DatumIzadavanja.BorderBrush = Brushes.Red;
            }
            else
            {
                DatumIzadavanja.BorderBrush = Brushes.Gray;
            }

            if (VaziDo.Text == "")
            {
                result = false;
                VaziDo.BorderBrush = Brushes.Red;
            }
            else
            {
                VaziDo.BorderBrush = Brushes.Gray;
            }

            if (mobilnostComboBox.SelectedItem == null)
            {
                result = false;
                mob.Foreground = Brushes.Red;
            }
            else
            {
                mob.Foreground = Brushes.Black;
            }

            if (szrComboBox.SelectedItem == null)
            {
                result = false;
                spo.Foreground = Brushes.Red;
            }
            else
            {
                spo.Foreground = Brushes.Black;
            }

            if (smestajComboBox.SelectedItem == null)
            {
                result = false;
                sml.Foreground = Brushes.Red;
            }
            else
            {
                sml.Foreground = Brushes.Black;
            }

            return result;
        }

        private void Load_Data_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (Interop interop = new Interop())
                {
                    interop.Startup();
                    CardData cardData = interop.Read();
                    pom = cardData.PortraitData.portrait;
                    pomSize = cardData.PortraitData.portraitSize;
                    Portrait p = new Portrait(cardData.PortraitData);
                    PortraitImage.Source = BitmapToImageSource(p.PortraitImg);
                    Prezime.Text = cardData.FixedData.surname;
                    Ime.Text = cardData.FixedData.givenName;
                    ImeRoditelja.Text = cardData.FixedData.parentGivenName;
                    DatumRodjenja.Text = cardData.FixedData.dateOfBirth;
                    MestoOpstinaDrzava.Text = cardData.FixedData.placeOfBirth + ", " +
                        cardData.VariableData.community + ", " + cardData.FixedData.stateOfBirth;
                    Prebivaliste.Text = cardData.VariableData.community + ", " + cardData.VariableData.place + ", " +
                        cardData.VariableData.street + " " + cardData.VariableData.houseNumber + "/" + cardData.VariableData.apartmentNumber;
                    Pol.Text = cardData.FixedData.sex;
                    Izdaje.Text = cardData.DocumentData.issuingAuthority;
                    DatumIzadavanja.Text = cardData.DocumentData.issuingDate;
                    VaziDo.Text = cardData.DocumentData.expiryDate;
                    interop.Cleanup();
                }
            }
            catch (InvalidApiVersionException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (ReadingFailedException ex)
            {
                Console.WriteLine("Greska prilikom citanja sa kartice. Poziv {0} nije uspeo, kod greske: {1}", ex.FailedAction, ex.Result);
            }
        }

        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            listAllUsersPom.Clear();
            DataAccess dt = new DataAccess();
            listAllUsers = dt.getAllUsers();
            foreach (Users u in listAllUsers)
            {
                if (u.smestaj == (string)smestajComboBox.SelectedItem)
                {
                    listAllUsersPom.Add(u.givenName + " " + u.surname + " (" + u.szr + ")");
                }
            }
            listaKorisnika.ItemsSource = listAllUsersPom;
            listaKorisnika.Items.Refresh();
        }
    }
}
