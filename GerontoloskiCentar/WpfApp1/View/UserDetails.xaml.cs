﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1.Data;
using WpfApp1.Model;

namespace WpfApp1.View
{
    /// <summary>
    /// Interaction logic for UserDetails.xaml
    /// </summary>
    public partial class UserDetails : Window
    {
        BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        public List<Rooms> lista = new List<Rooms>();

        public UserDetails(Users user)
        {
            InitializeComponent();
            ucitajPodatke(user);
        }

        private void ucitajPodatke(Users user)
        {
            PEID_PORTRAIT pPom = new PEID_PORTRAIT(user.portrait, user.portraitSize);
            Portrait p = new Portrait(pPom);
            PortraitImage.Source = BitmapToImageSource(p.PortraitImg);
            Prezime.Content = user.surname;
            Ime.Content = user.givenName;
            ImeRoditelja.Content = user.parentGivenName;
            DatumRodjenja.Content = user.dateOfBirth;
            MestoOpstinaDrzava.Content = user.mestoRodjenja;
            Jmbg.Content = user.personalNumber;
            Prebivaliste.Content = user.prebivaliste;
            Pol.Content = user.sex;
            Izdaje.Content = user.issuingAuthority;
            BrojDokumenta.Content = user.docRegNo;
            DatumIzadavanja.Content = user.issuingDate;
            VaziDo.Content = user.expiryDate;
            mobilnost.Text = user.mobilnost;
            szr.Text = user.szr;
            placanje.Text = user.placanje;
            smestaj.Text = user.smestaj;
            hobi.Text = user.hobi;
            ishrana.Text = user.ishrana;
            DataAccess dt = new DataAccess();
            lista = dt.getAllRooms();
            foreach (Rooms r in lista)
            {
                if (user.smestaj == r.nameRoom)
                    cenaSmestaja.Content = r.priceRoom;
            }
        }
    }
}
