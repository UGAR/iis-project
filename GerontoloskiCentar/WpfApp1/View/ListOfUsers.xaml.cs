﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1.Model;

namespace WpfApp1.View
{
    /// <summary>
    /// Interaction logic for ListOfUsers.xaml
    /// </summary>
    public partial class ListOfUsers : Window
    {
        public static List<Users> listOfUsers = new List<Users>();
        public static List<Users> lista = new List<Users>();

        public ListOfUsers()
        {
            InitializeComponent();
            getAllUsers();
        }

        private void getAllUsers()
        {
            DataAccess dt = new DataAccess();
            listOfUsers =  dt.getAllUsers();
            UsersGrid.ItemsSource = listOfUsers;
            UsersGrid.Items.Refresh();
        }

        private void ShowDetails(object sender, RoutedEventArgs e)
        {
            Users obj = ((FrameworkElement)sender).DataContext as Users;
            UserDetails u = new UserDetails(obj);
            u.Show();
            this.Close();
        }

        private void Pretrazi_Click(object sender, RoutedEventArgs e)
        {
            lista.Clear();
            UsersGrid.Items.Refresh();

            if ("" == ime.Text.ToLower() && "" == prezime.Text.ToLower() &&
                   "" == datumRodjenja.Text.ToLower() && "" == jmbg.Text.ToLower())
            {
                UsersGrid.ItemsSource = listOfUsers;
                UsersGrid.Items.Refresh();
            }
            else
            {
                foreach (Users u in listOfUsers)
                {
                    if (u.givenName.ToLower() == ime.Text.ToLower() && u.surname.ToLower() == prezime.Text.ToLower() &&
                        u.dateOfBirth.ToLower() == datumRodjenja.Text.ToLower() && u.personalNumber.ToLower() == jmbg.Text.ToLower())
                    {
                        lista.Add(u);
                    }
                }
                UsersGrid.ItemsSource = lista;
                UsersGrid.Items.Refresh();
            }
        }

        private void UpdateInfo(object sender, RoutedEventArgs e)
        {
            Users obj = ((FrameworkElement)sender).DataContext as Users;
            UpdateUser u = new UpdateUser(obj);
            u.Show();
            this.Close();
        }

        private void Delete(object sender, RoutedEventArgs e)
        {
            Users obj = ((FrameworkElement)sender).DataContext as Users;
            DeleteUser d = new DeleteUser(obj);
            d.Show();
            this.Close();
        }
    }
}
