﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1.Model;

namespace WpfApp1.View
{
    /// <summary>
    /// Interaction logic for ListOfRooms.xaml
    /// </summary>
    public partial class ListOfRooms : Window
    {
        RoomUserControl[] roomUserControls { get; set; }
        List<Rooms> listAllRooms = new List<Rooms>();

        public ListOfRooms()
        {
            InitializeComponent();
            ShowRooms();
        }

        private void ShowRooms()
        {
            DataAccess dt = new DataAccess();
            listAllRooms = dt.getAllRooms();
            roomUserControls = new RoomUserControl[listAllRooms.Count];
            for (int i = 0; i < listAllRooms.Count; i++)
            {
                roomUserControls[i] = new RoomUserControl();
                roomUserControls[i].Width = 1045;
                roomUserControls[i].obj = listAllRooms[i];
                roomUserControls[i].nameRoom.Content = listAllRooms[i].nameRoom;
                roomUserControls[i].typeRoom.Content = listAllRooms[i].typeRoom;
                roomUserControls[i].occupancyRoom.Content = listAllRooms[i].occupancyRoom.ToString() + "/" + listAllRooms[i].capacityRoom.ToString();
                ListOfRoomUserControl.Items.Add(roomUserControls[i]);
            }
        }
    }
}
