﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1.Model;

namespace WpfApp1.View
{
    /// <summary>
    /// Interaction logic for ChangeRoom.xaml
    /// </summary>
    public partial class ChangeRoom : Window
    {
        public List<Rooms> listAllRooms = new List<Rooms>();
        public List<Users> listAllUsers = new List<Users>();

        public ChangeRoom()
        {
            InitializeComponent();
            popuniComboBox();
            smestajComboBox.IsEnabled = false;
            promeniSobuButton.IsEnabled = false;
        }

        private void popuniComboBox()
        {
            DataAccess dt = new DataAccess();
            listAllRooms = dt.getAllRooms();
            listAllUsers = dt.getAllUsers();

            foreach (Users u in listAllUsers)
            {
                korisniciComboBox.Items.Add(u.givenName + " " + u.surname + " (" + u.docRegNo + ")");
            }
        }

        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            trenutniSmestaj.Text = "";
            smestajComboBox.Items.Clear();
            smestajComboBox.IsEnabled = true;
            promeniSobuButton.IsEnabled = true;

            foreach (Users u in listAllUsers)
            {
                foreach (Rooms r in listAllRooms)
                {
                    if ((string)korisniciComboBox.SelectedValue == u.givenName + " " + u.surname + " (" + u.docRegNo + ")")
                    {
                        trenutniSmestaj.Text = u.smestaj;
                    }

                    if(u.smestaj != r.nameRoom && (string)korisniciComboBox.SelectedValue == u.givenName + " " + u.surname + " (" + u.docRegNo + ")"
                        && r.occupancyRoom < r.capacityRoom)
                    {
                        smestajComboBox.Items.Add(r.nameRoom);
                        smestajComboBox.Items.Refresh();
                    }
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Validation())
            {
                Users pom = new Users();

                foreach (Users u in listAllUsers)
                {
                    if ((string)korisniciComboBox.SelectedValue == u.givenName + " " + u.surname + " (" + u.docRegNo + ")")
                    {
                        pom = u;
                    }
                }

                DataAccess dt = new DataAccess();
                dt.UpdateRoomForUser(pom, (string)smestajComboBox.SelectedValue);
                int novoStanje = 0;
                int novoStanje1 = 0;

                foreach (Rooms r in listAllRooms)
                {
                    if(r.nameRoom == (string)smestajComboBox.SelectedValue)
                    {
                        novoStanje = r.occupancyRoom + 1;
                    }
                }

                foreach (Rooms r in listAllRooms)
                {
                    if (r.nameRoom == pom.smestaj)
                    {
                        novoStanje1 = r.occupancyRoom - 1;
                    }
                }
                dt.updateOccupancyRoom((string)smestajComboBox.SelectedValue, novoStanje);
                dt.updateOccupancyRoom(pom.smestaj, novoStanje1);

                MessageBox.Show("Promenili ste sobu korisniku!");

                smestajComboBox.SelectedValue = null;
                korisniciComboBox.SelectedValue = null;
                trenutniSmestaj.Text = "";
                listAllUsers = dt.getAllUsers();
            }
        }

        private bool Validation()
        {
            bool result = true;

            if (smestajComboBox.SelectedValue == null)
            {
                result = false;
                noviSmestaj.Foreground = Brushes.Red;
            }
            else
            {
                noviSmestaj.Foreground = Brushes.Black;
            }

            return result;
        }
    }
}
