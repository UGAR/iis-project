﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{
    public class ReadingFailedException : Exception
    {
        public ReadingFailedException(string msg)
            : base(msg) { }

        public int Result { get; set; }

        public string FailedAction { get; set; }
    }
}
