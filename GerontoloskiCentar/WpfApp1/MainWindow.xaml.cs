﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp1;
using WpfApp1.Data;
using System.IO;
using System.Drawing;
using WpfApp1.View;
using WpfApp1.AdminView;
using Brushes = System.Windows.Media.Brushes;
using WpfApp1.Model;

namespace projekat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Workers> lista = new List<Workers>();

        public MainWindow()
        {
            InitializeComponent();
            DataAccess dt = new DataAccess();
            lista = dt.getAllWorkers();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Validation())
            {
                Workers work = new Workers();
                DataAccess dt = new DataAccess();
                foreach(Workers w in lista)
                {
                    if(w.username == username.Text && w.passwordWorker == password.Password.ToString())
                    {
                        work = w;
                    }
                }

                if(work != null && work.placeWorker != null)
                {
                    if (work.placeWorker.ToLower() == "upravnik")
                    {
                        Upravnik u = new Upravnik();
                        u.Show();
                        this.Close();
                    }
                    else if (work.placeWorker.ToLower() == "admin")
                    {
                        Admin a = new Admin();
                        a.Show();
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Korisnik nije pronađen!");
                }
            }
        }

        private bool Validation()
        {
            bool result = true;

            if(username.Text == "")
            {
                result = false;
                username.BorderBrush = Brushes.Red;
            }
            else
            {
                username.BorderBrush = Brushes.Gray;
            }

            if (password.Password.ToString() == "")
            {
                result = false;
                password.BorderBrush = Brushes.Red;
            }
            else
            {
                password.BorderBrush = Brushes.Gray;
            }

            return result;
        }
    }
}
