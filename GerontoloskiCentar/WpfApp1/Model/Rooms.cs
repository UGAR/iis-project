﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Model
{
    public class Rooms
    {
        public string nameRoom { get; set; }
        public string typeRoom { get; set; }
        public string priceRoom { get; set; }
        public int capacityRoom { get; set; }
        public int occupancyRoom { get; set; }

        public Rooms(string nameRoom, string typeRoom, int capacityRoom, int occupancyRoom, string priceRoom)
        {
            this.nameRoom = nameRoom;
            this.typeRoom = typeRoom;
            this.capacityRoom = capacityRoom;
            this.occupancyRoom = occupancyRoom;
            this.priceRoom = priceRoom;
        }

        public Rooms() { }
    }
}
