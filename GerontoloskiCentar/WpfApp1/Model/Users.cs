﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Model
{
    public class Users
    {
        public string surname { get; set; }
        public string givenName { get; set; }
        public string sex { get; set; }
        public string parentGivenName { get; set; }
        public string dateOfBirth { get; set; }
        public string mestoRodjenja { get; set; }
        public string personalNumber { get; set; }
        public string prebivaliste { get; set; }
        public string issuingAuthority { get; set; }
        public string docRegNo { get; set; }
        public string issuingDate { get; set; }
        public string expiryDate { get; set; }
        public byte[] portrait { get; set; }
        public int portraitSize { get; set; }
        public string mobilnost { get; set; }
        public string szr { get; set; }
        public string placanje { get; set; }
        public string smestaj { get; set; }
        public string hobi { get; set; }
        public string ishrana { get; set; }

        public Users() { }

        public Users(string surname, string givenName, string sex, string parentGivenName, string dateOfBirth, string mestoRodjenja, string personalNumber, string prebivaliste, string issuingAuthority, string docRegNo, string issuingDate, string expiryDate, byte[] portrait, int portraitSize, string mobilnost, string szr, string placanje, string smestaj, string hobi, string ishrana)
        {
            this.surname = surname;
            this.givenName = givenName;
            this.sex = sex;
            this.parentGivenName = parentGivenName;
            this.dateOfBirth = dateOfBirth;
            this.mestoRodjenja = mestoRodjenja;
            this.personalNumber = personalNumber;
            this.prebivaliste = prebivaliste;
            this.issuingAuthority = issuingAuthority;
            this.docRegNo = docRegNo;
            this.issuingDate = issuingDate;
            this.expiryDate = expiryDate;
            this.portrait = portrait;
            this.portraitSize = portraitSize;
            this.mobilnost = mobilnost;
            this.szr = szr;
            this.placanje = placanje;
            this.smestaj = smestaj;
            this.hobi = hobi;
            this.ishrana = ishrana;
        }
    }
}
