﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Model
{
    public class Workers
    {
        public string nameWorker { get; set; }
        public string surnameWorker { get; set; }
        public string personalNumberWorker { get; set; }
        public string DocRegNoWorker { get; set; }
        public string prebivalisteWorker { get; set; }
        public string placeWorker { get; set; }
        public string username { get; set; }
        public string passwordWorker { get; set; }

        public Workers(string nameWorker, string surnameWorker, string personalNumberWorker, string docRegNoWorker, string prebivalisteWorker, string placeWorker, string username, string passwordWorker)
        {
            this.nameWorker = nameWorker;
            this.surnameWorker = surnameWorker;
            this.personalNumberWorker = personalNumberWorker;
            DocRegNoWorker = docRegNoWorker;
            this.prebivalisteWorker = prebivalisteWorker;
            this.placeWorker = placeWorker;
            this.username = username;
            this.passwordWorker = passwordWorker;
        }

        public Workers() { }
    }
}
